// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "BonusBase.generated.h"

class ASnakeGameGameModeBase;
class ASnakeBase;

UCLASS()
class SNAKEGAME_API ABonusBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonusBase();

	bool bNewBonusesTickInterval;
	float TickInterval;
	int Lifetime;
	int NumberOfTickToIndicateBeforeDestruction;

	UPROPERTY()
		ASnakeGameGameModeBase* ActualGameMode;

	ASnakeBase* ActualSnake;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	virtual void BonusesLifeCycle();
};
