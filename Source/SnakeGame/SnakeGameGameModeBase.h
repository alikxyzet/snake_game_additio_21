// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameGameModeBase.generated.h"

class AWall;
class AFood;
class ABonusBase;

USTRUCT()
struct FArrayBonuses
{
	GENERATED_BODY()

public:
	// �����
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABonusBase> BonusActorClass;

	// �������� ��� ������
	UPROPERTY(EditDefaultsOnly)
		int32 NumericalWeight;
};

UCLASS()
class SNAKEGAME_API ASnakeGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	int32 MaxRandomX;
	int32 MaxRandomY;

public:
	// Sets default values for this pawn's properties
	ASnakeGameGameModeBase();

	bool bBonusSpawn;

	ABonusBase* BonusesActor;

	TSubclassOf<APawn> ActualPawnClass;

	UPROPERTY(EditDefaultsOnly, Category = "Special")
		float OtherElementSize;

	UPROPERTY(EditDefaultsOnly, Category = "Special")
		TSubclassOf<AFood> FoodActorClass;

	UPROPERTY(BlueprintReadWrite)
		AWall* WallActor;

	UPROPERTY(EditDefaultsOnly, Category = "Special")
		TSubclassOf<AWall> WallActorClass;

	// "������� �� �����"
	UPROPERTY(EditDefaultsOnly, Category = "Special|Border of Walls")
		int32 HorizontalLength;

	UPROPERTY(EditDefaultsOnly, Category = "Special|Border of Walls")
		int32 VerticalLength;

	UPROPERTY(EditDefaultsOnly, Category = "Special|Border of Walls")
		int32 LocationHeight;

	// ��������� ������
	UPROPERTY(EditDefaultsOnly, Category = "Special|Bonus Settings")
		float BonusesTickInterval;

	UPROPERTY(EditDefaultsOnly, Category = "Special|Bonus Settings")
		int ChanceOfBonusForOneTick;

	UPROPERTY(EditDefaultsOnly, Category = "Special|Bonus Settings")
		int BonusLifetime;

	UPROPERTY(EditDefaultsOnly, Category = "Special|Bonus Settings")
		int NumberOfTickToIndicateBeforeDestruction;

	UPROPERTY(EditDefaultsOnly, Category = "Special|Bonus Settings")
		TArray<FArrayBonuses> Bonuses;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

	void CreateWallActor();

	UFUNCTION()
		void SpawnFoodActor();

	UFUNCTION()
		void SpawnBonusActor();
};
