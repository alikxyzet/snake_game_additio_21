// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()

	float TickInterval;

public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		float SnakeElementSize;

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;

	UPROPERTY(EditDefaultsOnly)
		float DeltaSpeed;

	UPROPERTY(EditDefaultsOnly)
		float MinSpeed;

	UPROPERTY(EditDefaultsOnly)
		float MaxSpeed;
	
	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
		int32 SnakeElementsInvulnerability;

	// ������� ���� "������" ��� ������� ���������
	// ����������: CurrentMoveDirection, NextMoveDirection
	// PS: ��� ��������� "������" �������� ������ � ������� ������ ����,
	//     "������" ��������������� �� �����
	UPROPERTY()
		EMovementDirection CurrentMoveDirection;
	UPROPERTY()
		EMovementDirection NextMoveDirection;
	//-------------------------------------------

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void AddSnakeElement(int ElementsNum = 1, bool NewSnake = false);
	UFUNCTION(BlueprintCallable)
		void Move();
	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
	UFUNCTION(BlueprintCallable)
		void NewSnakeSpeed(bool SpeedUp = false);
	UFUNCTION(BlueprintCallable)
		void SnakeInvulnerability();
};
