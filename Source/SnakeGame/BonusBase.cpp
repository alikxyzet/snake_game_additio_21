// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusBase.h"
#include "SnakeBase.h"
#include "SnakeGameGameModeBase.h"

// Sets default values
ABonusBase::ABonusBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	TickInterval = 1.f;
}

// Called when the game starts or when spawned
void ABonusBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(TickInterval);
}

// Called every frame
void ABonusBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorTickInterval(TickInterval);

	// ����������� ������� �����
	if (Lifetime <= 0)
	{
		BonusesLifeCycle();
	}
	else if (Lifetime == NumberOfTickToIndicateBeforeDestruction)
	{
		FVector ActualVector = this->GetActorScale3D() / 2;
		this->SetActorScale3D(ActualVector);
		Lifetime--;
	}
	else
	{
		Lifetime--;
	}
}

void ABonusBase::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
		BonusesLifeCycle();
}

void ABonusBase::BonusesLifeCycle()
{

	this->Destroy();
	// ���� ��������� "������" ���� ������� ����� GameMode,
	// �� ������� ��������� "�����"
	if (IsValid(ActualGameMode))
	{
		ActualGameMode->bBonusSpawn = false;
	}

}