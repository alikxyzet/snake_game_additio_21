// Copyright Epic Games, Inc. All Rights Reserved.


#include "SnakeGameGameModeBase.h"
#include "Components/InputComponent.h"
#include "Wall.h"
#include "Food.h"
#include "BonusBase.h"

// Sets default values
ASnakeGameGameModeBase::ASnakeGameGameModeBase()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HorizontalLength = 8;
	VerticalLength = 20;
	OtherElementSize = 100.f;
	ChanceOfBonusForOneTick = 50;
	BonusLifetime = 20;
	bBonusSpawn = false;
	BonusesTickInterval = 1.f;
	NumberOfTickToIndicateBeforeDestruction = 3;
}

void ASnakeGameGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(BonusesTickInterval);
	MaxRandomX = (HorizontalLength * 2) - 1;
	MaxRandomY = (VerticalLength * 2) - 1;

	CreateWallActor();
	SpawnFoodActor();
}

void ASnakeGameGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!bBonusSpawn)
	{
		SpawnBonusActor();
	}
}

void ASnakeGameGameModeBase::CreateWallActor()
{
	// ������������� ���������� ��� ������� �������� ����
	int32 PositiveHorizontally = HorizontalLength * OtherElementSize;
	int32 PositiveVertically = VerticalLength * OtherElementSize;
	FTransform WallElemTransform;

	// �������� ������� �������� ����
	for (int32 i = -HorizontalLength + 1; i < HorizontalLength; i++)
	{
		WallElemTransform = FTransform(FVector(i * OtherElementSize, PositiveVertically, LocationHeight));
		WallActor = GetWorld()->SpawnActor<AWall>(WallActorClass, WallElemTransform);

		WallElemTransform = FTransform(FVector(i * OtherElementSize, -PositiveVertically, LocationHeight));
		WallActor = GetWorld()->SpawnActor<AWall>(WallActorClass, WallElemTransform);
	}
	for (int32 i = -VerticalLength + 1; i < VerticalLength; i++)
	{
		WallElemTransform = FTransform(FVector(PositiveHorizontally, i * OtherElementSize, LocationHeight));
		WallActor = GetWorld()->SpawnActor<AWall>(WallActorClass, WallElemTransform);

		WallElemTransform = FTransform(FVector(-PositiveHorizontally, i * OtherElementSize, LocationHeight));
		WallActor = GetWorld()->SpawnActor<AWall>(WallActorClass, WallElemTransform);
	}
}

// ����� "���" � ��������� ����� � �������� �������� ����
void ASnakeGameGameModeBase::SpawnFoodActor()
{
	FVector2D RandomSpawnFood;

	RandomSpawnFood.X = rand() % MaxRandomX - (HorizontalLength - 1);
	RandomSpawnFood.Y = rand() % MaxRandomY - (VerticalLength - 1);

	FVector2D SpawnLocation = RandomSpawnFood * OtherElementSize;
	FTransform FoodElemTransform = FTransform(FVector(SpawnLocation.X, SpawnLocation.Y, LocationHeight + 10));;
	AFood* FoodActor = GetWorld()->SpawnActor<AFood>(FoodActorClass, FoodElemTransform);
	FoodActor->CallingGameMode = this;
}

// ����� "������" � ��������� ����� � �������� �������� ����
void ASnakeGameGameModeBase::SpawnBonusActor()
{
	int RandomBonusNumber = 1 + rand() % 100;	// 1 ... 100
	if (RandomBonusNumber <= ChanceOfBonusForOneTick)
	{

		// ����������� ��������������
		FVector2D RandomSpawnBonus;
		RandomSpawnBonus.X = rand() % MaxRandomX - (HorizontalLength - 1);
		RandomSpawnBonus.Y = rand() % MaxRandomY - (VerticalLength - 1);
		FVector2D SpawnLocation = RandomSpawnBonus * OtherElementSize;
		FTransform BonusElemTransform = FTransform(FVector(SpawnLocation.X, SpawnLocation.Y, LocationHeight + 10));;

		// ��������� ����������� ���� ������
		int32 SumBonusNumbers = 0;
		int32 LowerLimit = 0;
		int32 UpperLimit = 0;
		int32 BonusRandomNumber = 0;
		TSubclassOf<ABonusBase> RandomActorClass;

		for (int i = 0; i < Bonuses.Num(); i++)
		{
			if (IsValid(Bonuses[i].BonusActorClass))
				SumBonusNumbers += Bonuses[i].NumericalWeight;
		}

		if (SumBonusNumbers != 0)
			BonusRandomNumber = rand() % SumBonusNumbers;

		for (int i = 0; i < Bonuses.Num(); i++)
		{
			if (IsValid(Bonuses[i].BonusActorClass))
			{
				UpperLimit += Bonuses[i].NumericalWeight;
				if (LowerLimit <= BonusRandomNumber && BonusRandomNumber < UpperLimit)
					RandomActorClass = Bonuses[i].BonusActorClass;
				LowerLimit = UpperLimit;
			}
		}

		if (IsValid(RandomActorClass))
		{
			BonusesActor = GetWorld()->SpawnActor<ABonusBase>(RandomActorClass, BonusElemTransform);
			BonusesActor->ActualGameMode = this;
			BonusesActor->Lifetime = BonusLifetime;
			BonusesActor->TickInterval = BonusesTickInterval;
			BonusesActor->NumberOfTickToIndicateBeforeDestruction = NumberOfTickToIndicateBeforeDestruction;
			bBonusSpawn = true;
		}
	}
}