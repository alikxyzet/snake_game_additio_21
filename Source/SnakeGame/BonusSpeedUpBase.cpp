// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusSpeedUpBase.h"
#include "SnakeBase.h"

// Sets default values
ABonusSpeedUpBase::ABonusSpeedUpBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABonusSpeedUpBase::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ABonusSpeedUpBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABonusSpeedUpBase::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		ActualSnake = Cast<ASnakeBase>(Interactor);
		if (IsValid(ActualSnake))
		{
			BonusesLifeCycle();
			ActualSnake->NewSnakeSpeed(true);
		}
	}
}