// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SnakeElementSize = 100.f;
	MovementSpeed = 1.f;
	TickInterval = 1 / MovementSpeed;
	NextMoveDirection = EMovementDirection::DOWN;
	DeltaSpeed = 0.2f;
	MinSpeed = 0.2f;
	MaxSpeed = 10.f;
	SnakeElementsInvulnerability = 0;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	TickInterval = 1 / MovementSpeed;
	SetActorTickInterval(TickInterval);
	AddSnakeElement(5, true);

}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	CurrentMoveDirection = NextMoveDirection;
}

void ASnakeBase::AddSnakeElement(int ElementsNum, bool NewSnake)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		// ������� ���� � ���������� ������ �������� -- ��������
		FVector NewLocation(0, 0, -300);
		if (NewSnake)
			NewLocation = FVector(SnakeElements.Num() * SnakeElementSize, 0, 0);
		// PS:	���� �������� "������", �� ����������� �������� ���� �� ������;
		//		���� ����������� �������, �� �� ������������� ��������� ��� ������

		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (NextMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += SnakeElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= SnakeElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += SnakeElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= SnakeElementSize;
		break;
	}
	
	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableIterface = Cast<IInteractable>(Other);
		if (InteractableIterface)
		{
			InteractableIterface->Interact(this, bIsFirst);
		}
	}
}

// ��������� ��������
void ASnakeBase::NewSnakeSpeed(bool SpeedUp)
{
	if (SpeedUp)
		if (MovementSpeed - DeltaSpeed <= MaxSpeed)
			MovementSpeed += DeltaSpeed;
		else
			MovementSpeed = MaxSpeed;
	else
		if (MovementSpeed - DeltaSpeed >= MinSpeed)
			MovementSpeed -= DeltaSpeed;
		else
			MovementSpeed = MinSpeed;

	TickInterval = 1 / MovementSpeed;
	SetActorTickInterval(TickInterval);
}


// ������������
void ASnakeBase::SnakeInvulnerability()
{
	if (SnakeElements.Num() - 1 > SnakeElementsInvulnerability)
	{
		SnakeElementsInvulnerability++;
		SnakeElements[SnakeElementsInvulnerability]->SetInvulnerabilityElementType();
		SnakeElements[SnakeElementsInvulnerability]->ToggleCollision();
	}
}